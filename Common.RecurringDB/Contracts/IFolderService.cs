﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Common.RecurringDB.Contracts
{
    public interface IFolderService
    {
        Task<string> GetFolderNameIfExist(string url);
        Task<ICollection<string>> GetNestedFoldersPath(string path);
    }
}
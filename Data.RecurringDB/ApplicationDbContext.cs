﻿using Data.RecurringDB.Entities;
using Microsoft.EntityFrameworkCore;

namespace Data.RecurringDB
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Folder> Folders { get; set; }

        public ApplicationDbContext(DbContextOptions options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Folder>(entity =>
            {
                entity.ToTable("Folders");
                entity.HasKey(o => o.Id);
                entity.HasIndex(o => o.Name);
                entity.Property(o => o.Id).HasDefaultValueSql("NEWID()");
            });
        }

    }
}
﻿using System;

namespace Data.RecurringDB.Entities
{
    public abstract class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
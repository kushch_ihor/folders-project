﻿namespace Data.RecurringDB.Entities
{
    public class Folder: BaseEntity
    {
        public string Name { get; set; }
        public Folder ParentFolder { get; set; }
    }
}
﻿using System.Diagnostics;
using System.Threading.Tasks;
using Common.RecurringDB.Contracts;
using Microsoft.AspNetCore.Mvc;
using Web.RecurringDB.Models;

namespace Web.RecurringDB.Controllers
{
    public class HomeController : Controller
    {
        private readonly IFolderService _folderService;

        public HomeController(IFolderService folderService)
        {
            _folderService = folderService;
        }

        public async Task<IActionResult> Index(string url)
        {
            var viewFolderModel = await GetFolderViewModel(url);
            return View(viewFolderModel);
        }

        private async Task<FolderViewModel> GetFolderViewModel(string url)
        {            
            var folderName = await _folderService.GetFolderNameIfExist(url);
            if (string.IsNullOrEmpty(folderName)) return new FolderViewModel();

            var nestedFoldersUrls = await _folderService.GetNestedFoldersPath(url);
            return new FolderViewModel { Name = folderName, NestedFolders = nestedFoldersUrls };
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

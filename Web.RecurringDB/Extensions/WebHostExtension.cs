﻿using System;
using System.Collections.Generic;
using System.Linq;
using Data.RecurringDB;
using Data.RecurringDB.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace Web.RecurringDB.Extensions
{
    public static class WebHostExtension
    {
        public static IWebHost SeedDatabase(this IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var serviceProvider = scope.ServiceProvider;

                Seed(serviceProvider);
            }
            return host;
        }

        private static void Seed(IServiceProvider serviceProvider)
        {
            using (var context = serviceProvider.GetRequiredService<ApplicationDbContext>())
            {
                if (!context.Folders.Any())
                {
                    var rootFolder = new Folder {Name = "Creating Digital Images", ParentFolder = null};
                    var firstlevelChild1 = new Folder {Name = "Resources", ParentFolder = rootFolder};
                    var firstlevelChild2 = new Folder {Name = "Evidence", ParentFolder = rootFolder};
                    var firstlevelChild3 = new Folder {Name = "Graphic Products", ParentFolder = rootFolder};

                    var secondLevelChld1 = new Folder {Name = "Primary Sources", ParentFolder = firstlevelChild1};
                    var secondLevelChld2 = new Folder {Name = "Secondary Sources", ParentFolder = firstlevelChild1};
                    var secondlevelChild3 = new Folder {Name = "Process", ParentFolder = firstlevelChild3};
                    var secondlevelChild4 = new Folder {Name = "Final Product", ParentFolder = firstlevelChild3};

                    var foldersCollection = new List<Folder>
                    {
                        rootFolder,
                        firstlevelChild1,
                        firstlevelChild2,
                        firstlevelChild3,
                        secondLevelChld1,
                        secondLevelChld2,
                        secondlevelChild3,
                        secondlevelChild4
                    };
                    context.Folders.AddRange(foldersCollection);
                }
                context.SaveChanges();
            }
        }
    }
}
﻿using System.Collections.Generic;

namespace Web.RecurringDB.Models
{
    public class FolderViewModel
    {
        public FolderViewModel()
        {
            NestedFolders = new List<string>();
        }
        public string Name { get; set; }
        public ICollection<string> NestedFolders { get; set; }
    }
}
﻿using System;
using System.IO;
using Data.RecurringDB;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Web.RecurringDB.Extensions;

namespace Web.RecurringDB
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var context = services.GetRequiredService<ApplicationDbContext>();
                    context.Database.Migrate();
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred while running migration on the database.");
                    throw;
                }
            }

            host.SeedDatabase().Run();
        }
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
            .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var env = hostingContext.HostingEnvironment;
                    config.SetBasePath(Directory.GetCurrentDirectory());
                    var jsonSettings = string.IsNullOrEmpty(env.EnvironmentName)
                        ? "appsettings.json" 
                        : $"appsettings.{env.EnvironmentName}.json";
                    config.AddJsonFile(
                        jsonSettings, optional: true, reloadOnChange: true);
                }).ConfigureLogging(logging =>
                {
                    logging.AddDebug();
                    logging.AddConsole();
                    logging.AddEventSourceLogger();
                })
                .UseStartup<Startup>();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.RecurringDB.Contracts;
using Data.RecurringDB;
using Data.RecurringDB.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Web.RecurringDB.Services
{
    public class FoldersService : IFolderService
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<FoldersService> _logger;

        public FoldersService(ApplicationDbContext context, ILogger<FoldersService> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<string> GetFolderNameIfExist(string url)
        {
            _logger.LogDebug($"{nameof(GetFolderNameIfExist)} => Start");
            _logger.LogDebug($"{nameof(GetFolderNameIfExist)} => Url:{url}");
            var folderName = GetFolderNameFromUrl(url);
            var folder = (await _context.Folders.Include(o => o.ParentFolder)
                .ToListAsync()).FirstOrDefault(o => o.Name == folderName);

            if (folder == null) return null;
            var folderUrl = GetPath(folder);
            _logger.LogDebug($"{nameof(GetFolderNameIfExist)} => End");
            return folderUrl.Equals(url, StringComparison.InvariantCulture) ? folderName : null;
        }

        private string GetFolderNameFromUrl(string url)
        {
            if (string.IsNullOrEmpty(url)) return url;
            var splittedArray = url.Split('/');
            return splittedArray[splittedArray.Length - 1];
        }

        public async Task<ICollection<string>> GetNestedFoldersPath(string path)
        {
            _logger.LogDebug($"{nameof(GetNestedFoldersPath)} => Start");
            _logger.LogDebug($"{nameof(GetNestedFoldersPath)} => Path:{path}");

            var folders = await _context.Folders.Include(o => o.ParentFolder).ToListAsync();
            var resultPath = folders.FirstOrDefault(o => GetPath(o) == path);

            if (resultPath == null)
                return new List<string>();

            var nestedFolders = await _context.Folders.Include(o => o.ParentFolder)
                .Where(o => o.ParentFolder.Id == resultPath.Id).ToListAsync();

            _logger.LogDebug($"{nameof(GetNestedFoldersPath)} => End");
            return nestedFolders.Select(GetPath).ToList();
        }

        private string GetPath(Folder folder)
        {
            var result = folder.Name;
            while (folder.ParentFolder != null)
            {
                folder = folder.ParentFolder;
                result = string.Join('/', folder.Name, result);
            }
            return result;
        }
    }
}